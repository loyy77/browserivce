package com.shixihub.browservice;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
class BrowServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BrowServiceApplication.class, args);
    }

}
