package com.shixihub.browservice;

import lombok.Data;

/**
 * @author LiShixi 2021/4/16 8:47 下午
 */
@Data
public class EchartsConvertQuery {
    String option ;
    /**
     * 函数时的参数
     */
    String pageFunction;
    /**
     * 指定domId
     */
    String domId;
}
